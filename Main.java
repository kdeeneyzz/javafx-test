import javafx.application.Application;
import org.thymeleaf.context.Context;
import spark.ModelAndView;
import spark.Spark  ;
import spark.template.thymeleaf.ThymeleafTemplateEngine;
import java.util.HashMap;

public class Main {

    private static String linkPath;

    public static void main(String[] args) {

        // creates a unique link
        linkPath = sparkUtil.generateRandomStringSequence();
        
        // initializes web sockets
        Spark.webSocket("/websocket/" + linkPath, EchoWebSocket.class);

        // creates the page to send request to the frontend
        Spark.get(linkPath, ((request, response) -> {
            HashMap<String, Object> model = new HashMap<>();
            ModelAndView modelAndView = new ModelAndView(model, "quiz");
                return new ThymeleafTemplateEngine().render(modelAndView);
        }));

        Spark.init();

        //initializes the actual desktop web app
        Application.launch(FrontEnd.class, args);
    }

    public static String getLinkPath() {return linkPath;}
}